from abc import ABCMeta, abstractmethod
import requests
import json
from pull_request import PullRequest


class RepoHandler:
    @staticmethod
    def get_repo(repo):
        """Get RepoHost for remote origin

        Args:
        repo -- gitpython Repo object

        Returns:
            GitLab if hosted on Gitlab, Bitbucket is hosted on Bitbucket
        """
        url = repo.remotes.origin.url
        if "gitlab" in url:
            return GitLab(repo)
        elif "bitbucket" in url:
            return Bitbucket(repo)
        else:
            return False


class RepoHost:
    __metaclass__ = ABCMeta

    def __init__(self, repo):
        self.repo = repo
        self.auth = ()
        parsed = self.parse_remote(repo.remotes.origin.url)
        self.domain = parsed[0]
        self.slug = parsed[1]

    def parse_remote(self, url):
        """Parses remote url domain and slug based on protocol
        Example:
            git@bitbucket.org/ownername/reponame ->
                (bitbucket.org,ownername/reponame)

        """
        if "git@" not in url:
            domain = self.http_protocol(url)
        else:
            domain = self.ssh_protocol(url)
        return domain

    def http_protocol(self, url):
        parts = url.split("://")[1].split("/")
        domain = parts[0]
        slug = parts[1] + "/" + parts[2].replace(".git", "")
        return (domain, slug)

    def ssh_protocol(self, url):
        parts = url.split(":")
        domain = parts[0].replace("git@", "")
        slug = parts[1].replace(".git", "")
        return (domain, slug)

    def get_web_url(self):
        return 'https://' + self.domain + "/" + self.slug

    @abstractmethod
    def get_api_url(self):
        pass

    @abstractmethod
    def get_pr_list(self, uname):
        pass

    @abstractmethod
    def auth_requirements(self):
        pass

    @abstractmethod
    def set_auth(self, auth):
        pass


class GitLab(RepoHost):

    def get_api_url(self):
        return 'https://' + self.domain + "/api/v3/projects/" + self.slug.replace("/", "%2F")

    def make_request(self, url):
        return requests.get(url, headers={'PRIVATE-TOKEN': self.auth})

    def get_pr_list(self, uname):
        PRURL = self.get_api_url() + '/merge_requests'
        list = self.make_request(PRURL)
        body = json.loads(list.text)
        open_prs = []
        for item in body:
            assignee = item['assignee'] is not None
            is_user = assignee and item['assignee']['username'] == uname
            if assignee and is_user:
                open_prs.append(PullRequest(item['iid'], item['title']))
        return open_prs

    def auth_requirements(self):
        return [
                ("Gitlab Private Token", True),
            ]

    def set_auth(self, auth):
        self.auth = auth[0]


class Bitbucket(RepoHost):

    def get_api_url(self):
        return 'https://api.' + self.domain + "/2.0/repositories/" + self.slug

    def get_pr_list(self, uname):
        PRURL = self.get_api_url() + '/pullrequests'
        list = requests.get(PRURL, auth=self.auth)
        body = json.loads(list.text)
        ids = []
        for item in body['values']:
            ids.append(item['id'])

        if len(ids) == 0:
            return False

        open_prs = []
        for id in ids:
            r = requests.get(PRURL + "/" + str(id), auth=self.auth)
            body = json.loads(r.text)
            for item in body['participants']:
                if item['user']['username'] == uname:
                    open_prs.append(PullRequest(body['id'], body['title'], approval=item['approved']))
        return open_prs

    def auth_requirements(self):
        return [
                ('Bitbucket username', False),
                ('Bitbucket password', True)
                ]

    def set_auth(self, auth):
        self.auth = auth
