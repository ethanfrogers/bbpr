class PullRequest:
    
    def __init__(self, number, title, approval = False):
        self.number = number
        self.title = title
        self.approval = approval

    def fmtstring(self):
       return "#{number}: {title}".format(number=self.number, title=self.title)
