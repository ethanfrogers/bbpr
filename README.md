# BBPR

## Purpose
Figuring out what needs to be approved (by you) is difficult in Bitbucket. This tool makes it suck less.

BBPR is a command line utility for figuring out what open Pull Requests you are a reviewer on that you haven't approved. Instead of clicking through all the open Pull Requests, use BBPR to quickly decide what needs to be done.

BBPR also supports repos hosted on Gitlab with Github support coming soon.
## Usage

```bash
$ ./pr <project-collaborator> [-a]
```

BBPR will analyze the current repository for the origin url and ask Bitbucket for a list of all open pull requests. You will be presented with the list of Pull Requests for `<project-collaborator>` that have yet to be approved. Optionally, `-a` will include approved Pull Requests in the list.

## Dependencies

```bash
$ pip install gitpython requests click
```